namespace tzr.Common.Models;

public class RenameOrMoveModel
{
    public string OldPath { get; set; }
    public string NewPath { get; set; }
}