using tzr.Common.Enums;

namespace tzr.Common.Models;

public class FileSystemEntryModel
{
    public string Name { get; set; }
    public FileSystemEntryType Type { get; set; }
    public DateTime LastModified { get; set; }
    public string[] Permissions { get; set; }
    public long? Size { get; set; }
    public bool? IsDirectoryEmpty { get; set; }
}