namespace tzr.Common.Enums;

public enum FileSystemEntryType
{
    File,
    Directory
}