namespace tzr.WebAPI.Helpers;

public static class FileHelper
{
    public static long GetFileSizeInBits(FileInfo fileInfo)
    {
        return fileInfo.Length * 8;
    }

    public static bool IsPathCorrect(string basePath, string path, out string gluedPath)
    {
        gluedPath = Path.GetFullPath(Path.Combine(basePath, path));

        return gluedPath.StartsWith(basePath);
    }
}