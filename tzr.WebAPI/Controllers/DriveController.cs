using Microsoft.AspNetCore.Mvc;
using Mono.Unix;
using tzr.Common.Enums;
using tzr.Common.Models;
using tzr.WebAPI.Helpers;

namespace tzr.WebAPI.Controllers;

[ApiController]
[Route("api/drive")]
public class DriveController : ControllerBase
{
    private readonly string _basePath;

    public DriveController(IConfiguration configuration)
    {
        _basePath = configuration["BasePath"];
        // Loggera nie wstrzykiwałem, bo to tylko demko...
    }


    [HttpGet]
    public ActionResult GetDictionariesAndFiles([FromQuery] string path = "")
    {
        // Sprawdzam czy nie ma sztuczek z próbą wyjścia z katalogu root np. ../../../etc/passwd
        if (!FileHelper.IsPathCorrect(_basePath, path, out var fullPath)) 
            return BadRequest("Invalid path.");
        //

        try
        {
            if (Directory.Exists(fullPath))
            {
                var files = Directory.GetFiles(fullPath)
                    .Select(filePath => new FileSystemEntryModel
                    {
                        Name = Path.GetFileName(filePath),
                        Type = FileSystemEntryType.File,
                        LastModified = System.IO.File.GetLastWriteTime(filePath),
                        Permissions = new UnixFileInfo(filePath).FileAccessPermissions.ToString().Split(", "),
                        Size = FileHelper.GetFileSizeInBits(new FileInfo(filePath))
                    });

                var directories = Directory.GetDirectories(fullPath)
                    .Select(dirPath => new FileSystemEntryModel
                    {
                        Name = Path.GetFileName(dirPath),
                        Type = FileSystemEntryType.Directory,
                        LastModified = Directory.GetLastWriteTime(dirPath),
                        Permissions = new UnixFileInfo(dirPath).FileAccessPermissions.ToString().Split(", "),
                        IsDirectoryEmpty = !Directory.EnumerateFileSystemEntries(dirPath).Any()
                    });

                return Ok(files.Concat(directories));
            }

            return NotFound($"Directory {path} does not exist.");
        }
        catch (Exception)
        {
            return StatusCode(500, "An error occurred while processing your request. Please try again later.");
        }
    }

    [HttpGet("download")]
    public IActionResult DownloadFile([FromQuery] string path, [FromQuery] bool open = false)
    {
        if (!FileHelper.IsPathCorrect(_basePath, path, out var fullPath)) 
            return BadRequest("Invalid path.");

        try
        {
            if (Directory.Exists(fullPath)) return BadRequest("The requested path points to a directory, not a file.");

            if (!System.IO.File.Exists(fullPath)) return NotFound();

            return open
                ? PhysicalFile(fullPath, "application/octet-stream")
                : PhysicalFile(fullPath, "application/octet-stream", Path.GetFileName(fullPath));
        }
        catch (Exception)
        {
            return StatusCode(500, "An error occurred while processing your request. Please try again later.");
        }
    }

    [HttpPost("rename-or-move")]
    public IActionResult RenameOrMove([FromBody] RenameOrMoveModel request)
    {
        if (!FileHelper.IsPathCorrect(_basePath, request.OldPath, out var oldFullPath))
            return BadRequest("Invalid old path.");

        if (!FileHelper.IsPathCorrect(_basePath, request.NewPath, out var newFullPath))
            return BadRequest("Invalid new path.");

        try
        {
            if (System.IO.File.Exists(oldFullPath))
                System.IO.File.Move(oldFullPath, newFullPath);
            else if (Directory.Exists(oldFullPath))
                Directory.Move(oldFullPath, newFullPath);
            else
                return NotFound();

            return Ok();
        }
        catch (Exception)
        {
            return StatusCode(500, "An error occurred while processing your request. Please try again later.");
        }
    }

    [HttpDelete]
    public IActionResult Delete([FromQuery] string path)
    {
        if (!FileHelper.IsPathCorrect(_basePath, path, out var fullPath)) 
            return BadRequest("Invalid path.");

        try
        {
            if (System.IO.File.Exists(fullPath))
            {
                System.IO.File.Delete(fullPath);
            }
            else if (Directory.Exists(fullPath))
            {
                if (Directory.EnumerateFileSystemEntries(fullPath).Any()) return Conflict("Directory is not empty.");

                Directory.Delete(fullPath);
            }
            else
            {
                return NotFound();
            }

            return Ok();
        }
        catch (Exception)
        {
            return StatusCode(500, "An error occurred while processing your request. Please try again later.");
        }
    }
}