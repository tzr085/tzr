#!/bin/bash
cd tzr.WebAPI
dotnet run &
api_pid=$!

sleep 5

cd ../tzr.BlazorWebAssembly
dotnet run &
razor_pid=$!

trap "kill $api_pid $razor_pid" SIGINT SIGTERM

wait $api_pid $razor_pid
