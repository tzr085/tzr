# Zadanie Rekrutacyjne - File Explorer

**Autor:** Paweł Tabaka

## Wymagania

Zadanie deweloperskie:  Przeglądarka struktury dyskowej 
 
Wymagana funkcjonalność
 
1           Listowanie zawartości folderu (root od którego zaczynamy ma być parametrem konfiguracyjnym), prezentowane atrybuty:
 
a.            Rodzaj (plik/folder) w formie ikony
 
b.            Nazwa pliku/folderu
 
c.            Data modyfikacji
 
d.            Rozmiar (dla folderu puste)
 
e.            Atrybuty (readonly, systemowy itp.)
 
2           Akcje:
 
a.            Przejście do zawartości wskazanego folderu (tylko katalogi) – akcja domyślna (kliknięcie w nazwę)
 
b.            Pobieranie pliku (tylko pliki) – akcja domyślna (kliknięcie w nazwę), pobiera i otwiera plik w domyślnej aplikacji
 
c.            Zapisz (tylko plik) – akcja w formie ikony dla rekordu, pobranie i zapis (bez otwierania)
 
d.            Zmiana nazwy (pliku i foldery) – akcja w formie ikony, po wywołaniu okno z możliwością wpisania nazwy, podpowiada się nazwa dotychczasowa
 
e.            Usuń – (pliki i foldery) akcja w formie ikony, przed wykonaniem popup z prośbą o potwierdzenie zamiaru usunięcia, w przypadku folder walidacja czy jest pusty, jeżeli nie zablokowanie akcji z komunikatem
 
f.             Powrót do folderu nadrzędnego – akcja w formie pierwszego rekordu na liście [..], akcja niedostępna dla folderu root, nie podlega sortowaniu
 
3           Inne wymagania
 
a.            Aktualny kontekst – ponad listą prezentacja aktualnej ścieżki (np. /home/Dokumenty/Prezentacje), opcjonalnie przechodzenie do folderu po kliknięciu miejsca na ścieżce
 
b.            Możliwość sortowania zawartości folderu po wszystkich kolumnach
 
FrontEnd – dowolna technologia.
 
Usługi - .NET Framework lub .NET Core.
 
W ramach zadania należy przygotować krótki plik Readme w formacie MarkDown, w którym należy opisać sposób uruchomienia i konfiguracji przygotowanej aplikacji.

## Wykonanie i struktura projektu

Zadanie zrealizowałem dla systemów z rodziny Unix z tego powodu, że nie miałem na żadnym metalu zainstalowanego Windowsa. W takim przypadku zamiast windowsowych attributes podałem unixowe permissions. Projekt pisałem na distro opartym na Fedorze 38. Inforumję w razie "W" gdyby tylko u mnie działało :)

tzr.WebAPI - (.NET 7) API do pośredniego zarządzania plikami i katalogami na dysku.

tzr.BlazorWebAssembly - (Balazor + Bootstrap + trochę JavaScriptu, bo nie wszystkie sztuczki obsługuje jeszcze natywnie Blazor). Front do zarządzania plikami i katalogami na dysku. 
Zdecydowałem się na tę technologią frontentową ponieważ ostatnio bawiłem się WebAssembly i chciałem w końcu wykorzystać to w praktyce.

tzr.Common - (.NET 7) biblioteka, która dzieli wspólny kod używany w tzr.WebAPI i tzr.BlazorWebAssembly. Po to, aby nie powtarzać kodu.

## Wymagania potrzebne do uruchomienia projektu

System operacyjny: GNU/Linux lub MacOS(?)

.NET 7 SDK

## Konfiguracja i uruchomienie projektu

1. Sklonuj repozytorium:
```bash
git clone https://gitea.com/tzr085/tzr.git
```

2. Wejdź do tzr/tzr.WebAPI:
```bash
cd tzr/tzr.WebAPI
```

3. Ustaw katalog root w appsettings.json zmieniając pole BasePath.

4. Przejdź do katalogu głównego projektu:
```bash
cd ..
```

5. Uczyń skrypt start.sh wykonywalnym:
```bash
chmod +x start.sh
```

6. Wykonaj skrypt:
```bash
./start.sh
```

## Zasoby

[http://localhost:5085/swagger/index.html](http://localhost:5085/swagger/index.html) - swagger

[http://localhost:5196/](http://localhost:5196/) - front

## Co zrobiłbym lepiej gdybym miał więcej czasu?

Na pewno zrobiłbym refactoring kodu na froncie, bo wygląda okropnie i zabezpieczyłbym backend, aby użytkownik nie mógł wyjść spoza ścieżki root poprzez symlinki.

## Licencja

WTFPL
